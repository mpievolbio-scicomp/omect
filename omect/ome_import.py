import argparse
import logging
import datetime
import re
import glob
import threading
import traceback

import pandas
import numpy
import nibabel
import os
import sys
import configparser
import omero.clients
import omero.constants
import omero.model
import omero.cli
import getpass
from omero.gateway import BlitzGateway
from collections import namedtuple
from PIL import Image

arguments = namedtuple("arguments", ["server",
                                     "scan_dir",
                                     "username",
                                     "password",
                                     "targetuser",
                                     "targetgroup",
                                     "dryrun",
                                     "nii",
                                     "timestamp",
                                     "nfiles",
                                     ]
                       )
logging.basicConfig(level=logging.DEBUG)

def main(argv):

    scan_dir = argv.scan_dir
    username = argv.username
    password = argv.password
    timestamp = argv.timestamp
    server = argv.server
    targetuser = argv.targetuser
    targetgroup = argv.targetgroup
    number_of_parallel_files = argv.nfiles

    logger = logging.getLogger(__name__)
    formatter = logging.Formatter('%(asctime)s [%(levelname)s] %(module)s: %(message)s')
    handler = logging.FileHandler("ome_import_{}.log".format(timestamp))
    handler.setFormatter(formatter)
    handler.setLevel(logging.INFO)
    logger.addHandler(handler)

    logger.warning("OMERO importer starting up.")

    if not os.path.isdir(scan_dir):
        raise IOError("Directory {} does not exist.".format(scan_dir))

    # Strip trailing "/" if present
    if scan_dir[-1] =="/":
        scan_dir = scan_dir[:-1]

    scan_id = os.path.split(scan_dir)[1]

    # Get dir contents.
    raw_tifs, raw_log = get_raw(scan_dir, scan_id)
    rec_tifs, rec_log = get_rec(scan_dir, scan_id)

    tifs = raw_tifs + rec_tifs

    if len(tifs) == 0:
        raise IOError("No tifs found in %s or any subdirectories. Please check directory." % ( scan_dir))

    # Generate the nifti stack from all reconstructed tifs.
    thread = None
    if argv.nii:
        tifs_to_convert = [tif for tif in rec_tifs if re.match(r".*__rec[0-9]*.tif$", tif) is not None]
        thread = threading.Thread(
            group=None,
            target=tifs_to_nii,
            name='tifs_to_nii',
            args=tifs_to_convert
        )
        thread.start()

        # This should be threaded, including the upload.
        base = os.path.splitext(tifs_to_convert[-1])[0]
        nii_filename = base + ".nii.gz"
        # rec_nii = tifs_to_nii(tifs_to_convert)

    # Init dataset ids.
    ds_id = None
    # Connect to omero.
    if not argv.dryrun:
        with connect_to_omero(server, username, password, targetuser, targetgroup) as conn:
            # Create datasets.
            ds_id = create_dataset(conn, scan_id)
            
            conn.close()

    targets = {}
    for tif in tifs:
        targets[tif] = ds_id

    write_bulk_files(level="Dataset", targets=targets, scan_id=scan_id)

    ### Run the cli command.
    if not argv.dryrun:
        # Connect to omero.
        with connect_to_omero(server, username, password, targetuser, targetgroup) as conn:
            try:
                logger.info("Starting bulk import.")
                session_id = conn._getSessionId()
                omero.cli.argv(["omero", "import",
                                "--bulk", "bulk_{}.yaml".format(scan_id),
                                "--key", session_id,
                                "--server", server,
                                "--port", "4064",
                                "--skip", "checksum",
                                "--skip", "upgrade",
                                "--report",
                                "--email", "computing@evolbio.mpg.de",
                                "--parallel-upload", str(number_of_parallel_files),
                                "--upload",
                                "--transfer", "ln_s"
                                ]
                               )

                # Treat nifty file if requested.
                if argv.nii:
                    # First check if conversion still running, block if True.
                    if thread.is_alive():
                        thread.join()

                    # If conversion is finished, upload.
                    if not thread.is_alive():
                        omero.cli.argv(["omero", "import",
                                        "--key", session_id,
                                        "--server", server,
                                        "--port", "4064",
                                        "--skip", "checksum",
                                        "--skip", "upgrade",
                                        "--report",
                                        "--email", "computing@evolbio.mpg.de",
                                        "--upload",
                                        "--transfer", "ln_s",
                                        "-d", str(ds_id),
                                        nii_filename
                                        ]
                                       )

                annotate_dataset(
                    conn,
                    ds_id,
                    rec_log,
                    scan_id, timestamp)
        

            except Exception as ex:
                logging.error("Bulk import incomplete. Return 1.")
                logging.error("Original error message follows")
                logging.error("%s", str(ex))
                traceback.print_exc()

                return 1, ds_id
            
            finally:
                conn.close()
        
    else:
        logging.warning(""" DRY RUN. Please inspect bulk_{0:s}.yaml and targets_{0:s}.csv and the rerun the import.""".format(scan_id))

    handler.close()
    logger.removeHandler(handler)

    logger = None
    del logger

    logging.info("Import complete. Return 0.")

    return 0, ds_id


def tifs_to_nii(*tifs):
    logger = logging.getLogger(__name__)
    """ Compress tif images into a nifty stack (8bit depth)
    :param tifs: The paths to the tif files to compress.
    :type  tifs: list

    :warning: EXPERIMENTAL. USE AT OWN RISK. ALL TIFS ARE LOADED INTO MEMORY, THIS MAY LEAD TO
    EXCESSIVE RAM USAGE.
    :credits: Jacob Reinhold https://gist.github.com/jcreinhold/dfe43f54b6dfd8bb7e9f293c0007e15d
    """
    imgs = []
    for fn in tifs:
        img = numpy.asarray(Image.open(fn)).astype(numpy.int8).squeeze()
        if img.ndim != 2:
            raise Exception(f'Only 2D data supported. File {fn} has dimension {img.ndim}.')
        imgs.append(img)
    imgs = numpy.stack(imgs, axis=2)
    base = os.path.splitext(fn)[0]
    nii_filename = base+".nii.gz"
    nibabel.Nifti1Image(imgs, None).to_filename(nii_filename)


def write_bulk_files(level, targets, scan_id):
    logger = logging.getLogger(__name__)
    """ Generate csv for bulk import.
    :param level: The targeted omero hierarchical element
    :type level: str ("Project" | "Dataset")

    :param targets: Maps path to target object id
    :type  targets: dict
    """
    _df = pandas.DataFrame(columns=["target", "path"])
    _df["path"] = list(targets.keys())
    _df["target"] = ["{}:id:{}".format(level, id) for id in targets.values()]

    targets_fname = "targets_{}.csv".format(scan_id)
    _df.to_csv(targets_fname, sep=",", index=False, header=False)

    document = """---
path: {}
columns:
- target
- path
""".format(targets_fname)

    bulk_fname = "bulk_{}.yaml".format(scan_id)
    with open(bulk_fname, 'w') as fp:
        fp.write(document)

def create_dataset(connection,
                   dataset_name,
                   suffix="",
                   ):
    logger = logging.getLogger(__name__)
    # Use omero.gateway.DatasetWrapper:
    new_dataset = omero.gateway.DatasetWrapper(
        connection,
        omero.model.DatasetI()
    )
    if suffix != "":
        dataset_name = dataset_name +"_{}".format(suffix)

    new_dataset.setName(dataset_name)
    new_dataset.save()

    logger.info("Created dataset {} with ID {}.".format(dataset_name, new_dataset.id))

    return new_dataset.id

def get_raw(scan_dir, scan_id):
    logger = logging.getLogger(__name__)
    logger.info("Scanning directory %s.", scan_dir)
    raw_tifs = sorted(glob.glob(os.path.join(scan_dir,'{}_*.tif'.format(scan_id))))
    raw_log = os.path.join(scan_dir,'{}_.log'.format(scan_id))

    logger.info("Found %s tifs in %s.", len(raw_tifs), scan_dir)
    return raw_tifs, raw_log

def get_rec(scan_dir, scan_id):
    logger = logging.getLogger(__name__)
    rec_dir = os.path.join(scan_dir, scan_id+"_Rec")
    logger.info("Scanning directory %s.", rec_dir)
    rec_tifs = sorted(glob.glob(os.path.join(rec_dir,'{}_*.tif'.format(scan_id))))
    logger.info("Found %s tifs in %s.", len(rec_tifs), rec_dir)
    # Get the rec_spr.bmp and append it to list of images.
    rec_bmp = glob.glob(os.path.join(rec_dir, "{}__rec_spr.bmp".format(scan_id)))
    logger.info("Found %s *__rec_spr.bmp in %s.", len(rec_bmp), rec_dir)
    rec_tifs += rec_bmp
    rec_log = os.path.join(rec_dir,'{}__rec.log'.format(scan_id))

    return(rec_tifs, rec_log)

def connect_to_omero(server,
                     username,
                     password,
                     targetuser,
                     targetgroup,
                     ):
    logger = logging.getLogger(__name__)
    logger.info("Connecting to %s.", server)

    connection = BlitzGateway(username, password, host=server, group=targetgroup)
    connection_status = connection.connect()

    if connection_status:
        logger.info("Connection established.")
    else:
        logger.warning("Connection failed. Please check your omero username and password and internet connectivity.")

    if targetuser is not None:
        return connection.suConn(username=targetuser, group=targetgroup, ttl=60000*60*24)

    return connection

def get_config(log):
    logger = logging.getLogger(__name__)
    logger.info("Reading metadata from %s.", log)
    config = configparser.ConfigParser()
    config.read(log)

    return config


def annotate_dataset(connection, dataset_id, log, scan_id, timestamp, src_id=None):
    logger = logging.getLogger(__name__)
    logger.info("Annotating dataset %s.", dataset_id)
    config = get_config(log)
    for section in config.sections():
        section_key_values = []
        map_annotation = omero.gateway.MapAnnotationWrapper(connection)
        namespace = "{}/MouseCT/Skyscan/{}".format(connection.getWebclientHost(), section)
        map_annotation.setNs(namespace)
        for k,v in config[section].items():
            section_key_values.append((k,v))
        map_annotation.setValue(section_key_values)
        map_annotation.save()
        dataset = connection.getObject("Dataset", dataset_id)
        dataset.linkAnnotation(map_annotation)

    # Annotate with import details.
    import_annotation = omero.gateway.MapAnnotationWrapper(connection)
    namespace = "{}/MouseCT/".format(connection.getWebclientHost())
    import_annotation.setNs(namespace)
    annotations = [("MouseDB ID", scan_id),
              ("Import date", timestamp)]

    # Attach bulk import files as file annotations.
    if src_id is not None: # rec dataset, link and use file annotations from raw.
        annotations.append(("Raw Dataset ID", "http://{}/webclient/?show=dataset-{}".format(connection.getWebclientHost(), src_id)))

        # Attach file annotations found on raw dataset.
        for fann in connection.getObject("Dataset", src_id).listAnnotations(ns=namespace):
            if isinstance(fann, omero.gateway.FileAnnotationWrapper):
                fname = fann.getFile().getName()
                if fname in ["bulk_{}.yaml".format(scan_id), "targets_{}.csv".format(scan_id), dataset_id+"__rec.log"]:
                    dataset.linkAnnotation(fann)

    else:
        bulk_file = "bulk_{}.yaml".format(scan_id)
        bulk_file_annotation = connection.createFileAnnfromLocalFile(
            bulk_file,
            mimetype='text/plain',
            ns=namespace,
            desc="YAML file for bulk import of dataset {}".format(dataset_id)
        )
        dataset.linkAnnotation(bulk_file_annotation)

        targets_file = "targets_{}.csv".format(scan_id)
        targets_file_annotation = connection.createFileAnnfromLocalFile(
            targets_file,
            mimetype='text/plain',
            ns=namespace,
            desc="Bulk import csv file listing the files to be imported into dataset {}".format(dataset_id)
        )
        dataset.linkAnnotation(targets_file_annotation)

        log_file_annotation = connection.createFileAnnfromLocalFile(
            log,
            mimetype='text/plain',
            ns=namespace,
            desc="Reconstruction log file for dataset {}".format(dataset_id)
        )

        dataset.linkAnnotation(log_file_annotation)

    import_annotation.setValue(annotations)
    import_annotation.save()
    dataset = connection.getObject("Dataset", dataset_id)
    dataset.linkAnnotation(import_annotation)

    # Copy import annotation to each image in the dataset.
    for child in dataset.listChildren():
        child.linkAnnotation(import_annotation)

if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    YN_to_bool = {"y":True,
                  "n":False}
    bool_to_YN = dict([(v,k) for k,v in YN_to_bool.items()])

    # Seed parameter.
    parser.add_argument("-u",
                        "--username",
                        dest="username",
                        help="omero login username",
                        default=None,
                        required=False,
                        type=str)

    parser.add_argument("-p",
                        "--password",
                        dest="password",
                        help="omero login password",
                        default=None,
                        required=False,
                        type=str)

    parser.add_argument("-i",
                        "--interactive",
                        dest="interact",
                        help="""Toggle interactive mode.""",
                        default=False,
                        const=True,
                        action="store_const",
                        )

    parser.add_argument("-U",
                        "--targetuser",
                        dest="targetuser",
                        help="""For which omero user to import this dataset""",
                        default=None,
                        type=str,
                        metavar="TARGETUSER",
                        required=False,
                        )

    parser.add_argument("-G",
                        "--targetgroup",
                        dest="targetgroup",
                        help="""For which omero group to import this dataset""",
                        default=None,
                        type=str,
                        metavar="TARGETGROUP",
                        required=False,
                        )

    parser.add_argument("-d",
                        "--directory",
                        dest="scan_dir",
                        help="""Path to the scan directory.""",
                        default=os.getcwd(),
                        type=str,
                        metavar="DIR",
                        required=False,
                        )
    parser.add_argument("-s",
                        "--server",
                        help="""The OMERO server URL""",
                        default="ome.evolbio.mpg.de",
                        type=str,
                        metavar="URL",
                        required=False,
                        )
    parser.add_argument("-n",
                        "--dryrun",
                        dest="dryrun",
                        help="""Parse directory and write the bulk import files but do not actually perform the upload and annotation.""",
                        default=False,
                        action='store_const',
                        const=True
                        )
    parser.add_argument("-N",
                        "--nii",
                        dest="nii",
                        help="""Run nifty compression and upload the nifty.gz file.""",
                        default=False,
                        action='store_const',
                        const=True
                        )
    parser.add_argument("-@",
                        "--nfiles",
                        dest="nfiles",
                        help="""Number of parallel file uploads""",
                        type=int,
                        metavar="NFILES",
                        default=1,
                        required=False,
                        )
    # Parse the arguments.
    args = parser.parse_args()

    ##
    print("*******************************")
    print("*                             *")
    print("* The Omero Mouse CT importer *")
    print("*                             *")
    print("*******************************")

    if args.username is None:
        username = getpass.getuser()
        tmp = input("Omero username [{}]:".format(username))
        if tmp != "":
            username = tmp
    else:
        username=args.username

    if args.password is None:
        password = getpass.getpass("Password for user {}: ".format(username))
    else:
        password=args.password

    nii = args.nii
    scan_dir = args.scan_dir
    dryrun = args.dryrun
    server = args.server
    targetuser = args.targetuser
    targetgroup = args.targetgroup
    nfiles=args.nfiles

    if args.interact:
        tmp = input("Scan directory [{}]: ".format(scan_dir))
        if tmp != "":
            scan_dir = tmp

        tmp = input("Generate nifti from reconstruction tifs? WARNING: EXPERIMENTAL FEATURE, MAY CAUSE MEMORY ISSUES! (y|n) [{}]: ".format(bool_to_YN[nii]))
        if tmp != "":
            nii = YN_to_bool[tmp.lower()]

        tmp = input("Dry run (y|n) [{}]: ".format(bool_to_YN[dryrun]))
        if tmp != "":
            dryrun = YN_to_bool[tmp.lower()]

        tmp = input("OMERO Server [ome.evolbio.mpg.de]: ")
        if tmp != "":
            server = tmp

    argv = arguments(scan_dir=scan_dir,
                     username=username,
                     targetgroup=targetgroup,
                     targetuser=targetuser,
                     password=password,
                     dryrun=dryrun,
                     nii=nii,
                     timestamp=datetime.datetime.now().isoformat(' ', 'seconds'),
                     server=server,
                     nfiles=nfiles,
                     )

    # Check scan_dir
    if scan_dir is None or scan_dir == "":
        raise IOError("Please provide a directory to scan.")

    sys.exit(main(argv))

