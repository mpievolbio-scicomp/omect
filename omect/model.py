# model.py
#
import os

class Model(object):
    def __init__( self ):
        '''
        '''
        self.__server = None
        self.__scan_dir = None
        self.__username = None
        self.__password = None
        self.__dryrun = True
        self.__nifti = False

    @property
    def server(self):
        return self.__server

    @server.setter
    def server(self, val):
        self.__server = val

    @property
    def scan_dir(self):
        return self.__scan_dir
    @scan_dir.setter
    def scan_dir(self, val):
        self.__scan_dir = val

    @property
    def username(self):
        return self.__username

    @username.setter
    def username(self, val):
        self.__username = val

    @property
    def password(self):
        return self.__password

    @password.setter
    def password(self, val):
        self.__password = val

    @property
    def nifti(self):
        return self.__nifti

    @nifti.setter
    def nifti(self, val):
        self.__nifti = val

    @property
    def dryrun(self):
        return self.__dryrun

    @dryrun.setter
    def dryrun(self, val):
        self.__dryrun = val

