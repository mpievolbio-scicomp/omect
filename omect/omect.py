import time

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import QObject, pyqtSlot, pyqtSignal, QThread
from PyQt5.QtWidgets import QDialogButtonBox

from omect_ui import Ui_Dialog
import sys
import getpass

import datetime
import os
from model import Model

import ome_import as omi
import logging

class WindowLogHandler(logging.Handler, QObject):
    """ Monitor given files."""

    def __init__(self):
        logging.Handler.__init__(self)
        QObject.__init__(self)

        self.__finished = False

    log_update = pyqtSignal(str)
    finished = pyqtSignal()

    def emit(self, log):
        msg = str(log.getMessage())
        self.log_update.emit(msg)

class Worker(QObject):
    """
    https://realpython.com/python-pyqt-qthread/
    """

    def __init__(self, args):
        super().__init__()

        self.__args = args

    finished = pyqtSignal(tuple)

    def run(self):
        status = None, (None, None)
        try:
            status = omi.main(self.__args)

        except:
            status = 2,(None, None)

        self.finished.emit(status)

class Omect(Ui_Dialog):
    def __init__( self ):
        '''Initialize the super class
        '''
        super().__init__()
        self.model = Model()


    def setupUi( self, main_window ):
        ''' Setup the UI of the super class, and add here code
        that relates to the way we want our UI to operate.
        '''
        super().setupUi( main_window )

        self.progressBar.setVisible(False)

        if self.model.server in ["", None]:
            self.model.server = "ome.evolbio.mpg.de"
        self.model.username=getpass.getuser()
        self.model.nifti=False
        self.model.dryrun=True

        log_handler = WindowLogHandler()
        ome_importer_logger = logging.getLogger(name="ome_import")
        log_handler.log_update.connect(self.log_browser.append)
        ome_importer_logger.addHandler(log_handler)
        self.refreshAll()

    def debugPrint( self, msg ):
        '''Print the message in the text edit at the bottom of the
        horizontal splitter.
        '''
        self.log_browser.append(msg)
    def log_update(self, log):
        self.log_browser.setText(log)

    def reset_after_import(self):
        self.model.scan_dir = ""
        self.refreshAll()

    def status_report(self, status):
        if status[0] == 0:
            self.debugPrint("Import job complete.")
            self.debugPrint(
                "\nRaw dataset:\nid:{}\nURL:http://{}/webclient/?show=dataset-{}\n".format(
                    status[1][0],
                    self.server.text(),
                    status[1][0]
                )
            )
            self.debugPrint(
                "\nRec dataset:\nid:{}\nURL:http://{}/webclient/?show=dataset-{}\n".format(
                    status[1][1],
                    self.server.text(),
                    status[1][1]
                )
            )
        elif status[0] == 1:
            self.debugPrint(
                "Import job partially complete. Please check datasets.\n\tRaw dataset id:\t{}\n\tRec dataset id:\t{}\n".format(status[1][0],
                                                                                                  status[1][1]))
        else:
            self.debugPrint("Import job failed. Please check scan directory and consult the log file.\n")

    def refreshAll( self ):
        '''
        Updates the widgets whenever an interaction happens.
        Typically some interaction takes place, the UI responds,
        and informs the model of the change.  Then this method
        is called, pulling from the model information that is
        updated in the GUI.
        '''
        self.server.setText(self.model.server)
        self.scan_dir.setText( self.model.scan_dir )
        self.username.setText(self.model.username)
        self.password.setText(self.model.password)
        self.nifti.setCheckState(self.model.nifti)
        self.dryrun.setCheckState(self.model.dryrun)

    def browseSlot( self ):
        ''' Called when the user presses the Browse button
        '''
        scan_dir = QtWidgets.QFileDialog.getExistingDirectory(
            None,
            "Select Directory",
            os.getcwd(),
        )
        if scan_dir:
            self.model.scan_dir = scan_dir
            self.refreshAll()
            self.debugPrint("Selected scan dir {}\n".format(scan_dir))


    def accept(self):
        """ Run the importer """
        self.buttonBox.button(QDialogButtonBox.Ok).setVisible(False)
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(False)
        self.buttonBox.button(QDialogButtonBox.Ok).setUpdatesEnabled(False)

        self.model.server = self.server.text()
        self.model.scan_dir  = self.scan_dir.text()
        self.model.server = self.server.text()
        self.model.username  = self.username.text()
        self.model.password  = self.password.text()
        self.model.dryrun    = self.dryrun.isChecked()
        self.model.nifti     = self.nifti.isChecked()

        args = omi.arguments(
            server=self.model.server,
            scan_dir=self.model.scan_dir,
            username=self.model.username,
            password=self.model.password,
            dryrun=self.model.dryrun,
            nii=self.model.nifti,
            timestamp=datetime.datetime.now().isoformat(' ', 'seconds'),
        )

        self.debugPrint("""
Running `ome_import` with 
    server = {}
    scan_dir = {}
    username = {}
    password = **********
    dryrun = {}
    nii = {}
    timestamp = {}
""".format(args.server, args.scan_dir, args.username, args.dryrun, args.nii, args.timestamp))

        self.refreshAll()

        self.worker_thread = QThread()
        self.worker_thread.setTerminationEnabled(True)
        self.worker = Worker(args)
        self.worker.moveToThread(self.worker_thread)
        self.worker_thread.started.connect(self.worker.run)
        self.worker.finished.connect(self.worker_thread.quit)
        self.worker.finished.connect(self.worker.deleteLater)
        self.worker_thread.finished.connect(self.worker_thread.deleteLater)
        self.worker.finished.connect(self.status_report)
        self.worker.finished.connect(self.reset_after_import)
        self.worker_thread.finished.connect(self._enable_ok)
        self.buttonBox.rejected.connect(self.delete_worker)
        self.buttonBox.rejected.connect(self.worker_thread.terminate)
        self.worker_thread.start()

        # self.monitor_thread = QThread()
        # self.monitor = FileMonitor(self.logfile)
        # self.monitor.moveToThread(self.monitor_thread)
        # self.monitor_thread.started.connect(self.monitor.run)
        # self.monitor.finished.connect(self.monitor_thread.quit)
        # self.monitor.finished.connect(self.monitor.deleteLater)
        # self.monitor_thread.finished.connect(self.monitor_thread.deleteLater)
        # self.monitor.file_update.connect(self.log_update)
        # self.worker.finished.connect(self.monitor.finish)
        # self.monitor_thread.start()

    def delete_worker(self):
        self.worker.finished.emit((2,(None, None)))
        self.worker = None
        del self.worker

    def _enable_ok(self):
        self.buttonBox.button(QDialogButtonBox.Ok).setVisible(True)
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(True)
        self.buttonBox.button(QDialogButtonBox.Ok).setUpdatesEnabled(True)



def main():
    """
    """
    app = QtWidgets.QApplication(sys.argv)
    main_window = QtWidgets.QMainWindow()
    ui = Omect()
    ui.setupUi(main_window)
    main_window.show()
    sys.exit(app.exec_())

main()

