from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import QObject, pyqtSlot
from scandir_ui import Ui_Dialog
import sys

import datetime
import os
from model import Model

from ome_import import main as omi
from ome_import import arguments

class ScanDirDialog(Ui_Dialog):
    def __init__( self ):
        '''Initialize the super class
        '''
        super().__init__()
        self.model = Model()

    def setupUi( self, main_window ):
        ''' Setup the UI of the super class, and add here code
        that relates to the way we want our UI to operate.
        '''
        super().setupUi( main_window )

    def debugPrint( self, msg ):
        '''Print the message in the text edit at the bottom of the
        horizontal splitter.
        '''
        pass

    def refreshAll( self ):
        '''
        Updates the widgets whenever an interaction happens.
        Typically some interaction takes place, the UI responds,
        and informs the model of the change.  Then this method
        is called, pulling from the model information that is
        updated in the GUI.
        '''
        self.lineEdit.setText( self.model.getDirName() )

    def browseSlot( self ):
        ''' Called when the user presses the Browse button
        '''
        dirName = QtWidgets.QFileDialog.getExistingDirectory(
            None,
            "Select Directory",
            os.getcwd(),
        )
        if dirName:
            self.model.setDirName(dirName)
            self.refreshAll()

    def accept(self):
        """ Run the importer """

        scan_dir = self.model.getDirName()
        username = "grotec"
        password = "f~2L4nG$"
        dryrun = True
        nii = False

        args = arguments(
            scan_dir=scan_dir,
            username=username,
            password=password,
            dryrun=dryrun,
            nii=nii,
            timestamp=datetime.datetime.now().isoformat(' ', 'seconds')
        )

        omi(args)



def main():
    """
    """
    app = QtWidgets.QApplication(sys.argv)
    main_window = QtWidgets.QMainWindow()
    ui = ScanDirDialog()
    ui.setupUi(main_window)
    main_window.show()
    sys.exit(app.exec_())

main()

