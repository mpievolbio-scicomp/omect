import sys
import os
from collections import namedtuple
import omero

test_dir = os.path.abspath(os.path.dirname(__file__))
root_dir = os.path.dirname(test_dir)
print(root_dir)
module_dir = os.path.join(root_dir, 'omect')
sys.path.insert(0, root_dir)

from omect import ome_import as omi
from omect.ome_import import arguments
import getpass




def test_scandir_raise():

    argv = arguments(scan_dir="/home/grotec/Projects/MouseCT/PedMap/50093960")
    raises = False
    try:
        omi.main(argv)
    except:
        raises = True

    assert raises


def test_rec_items():

    argv = arguments(scan_dir="/home/grotec/Projects/MouseCT/PedMap/50093961")

    scan_dir = argv.scan_dir
    scan_id = os.path.split(scan_dir)[1]

    rec_tifs, rec_log = omi.get_rec(scan_dir, scan_id)

    assert isinstance(rec_tifs, list)
    for rec_tif in rec_tifs:
        assert os.path.isfile(rec_tif)

    assert os.path.isfile(rec_log)


def test_raw_items():

    argv = arguments(scan_dir="/home/grotec/Projects/MouseCT/PedMap/50093961")

    scan_dir = argv.scan_dir
    scan_id = os.path.split(scan_dir)[1]

    raw_tifs, raw_log = omi.get_raw(scan_dir, scan_id)

    assert isinstance(raw_tifs, list)
    for raw_tif in raw_tifs:
        assert os.path.isfile(raw_tif)

    assert os.path.isfile(raw_log)


def test_write_bulk_files():

    level = "Dataset"
    targets = dict([("test_1.tif", 1), ("test_2.tif", 2)])

    omi.write_bulk_files(level, targets)

    assert "targets.csv" in os.listdir()
    assert "bulk.yaml" in os.listdir()

if __name__ == "__main__":

    username = getpass.getuser()
    password = getpass.getpass()
    argv = arguments(scan_dir=os.path.join(test_dir, "test_data", "50093962"),
                     username=username,
                     password=password,
                     dryrun=False,
                     nii=False,
                     server="ome.evolbio.mpg.de",
                     targetuser=None,
                     targetgroup=None,
                     timestamp="XXX",
                     nfiles=1,
                     )
    omi.main(argv)
